defmodule Orion.Parsers.Titles do
  require Logger

  alias Orion.Schemas.CalendarEvent
  alias Orion.Repo

  import Orion.Parsers.Helpers

  def parse(client, params \\ %{}) do
    case client.get(params) do
      {:ok, %{"data" => data}} ->
        mapped = mapify_events(Repo.all(CalendarEvent))

        new_events =
          data
          |> Enum.map(&create_or_update(&1, mapped))
          |> Enum.reject(&is_nil/1)

        clear_old_data(mapped, new_events, :title)

        new_events

      {:error, status, %{reason: reason}} ->
        Logger.error(
          "Could not connect to titles service, received #{status} status with reason: #{reason}"
        )
    end
  end

  defp create_or_update(%{"movie_id" => nil}, _current_events), do: nil

  defp create_or_update(data = %{"movie_id" => movie_id, "title" => title}, current_events) do
    found = current_events[movie_id]

    case found do
      nil ->
        insert_record(data)

      record ->
        case Map.get(record, :title) == title do
          true -> record
          false -> update_record(record, :title, title)
        end
    end
  end
end
