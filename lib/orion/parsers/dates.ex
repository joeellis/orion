defmodule Orion.Parsers.Dates do
  require Logger

  alias Orion.Schemas.CalendarEvent
  alias Orion.Repo

  import Orion.Parsers.Helpers

  def parse(client, params \\ %{}) do
    case client.get(params) do
      {:ok, %{"data" => data}} ->
        mapped = mapify_events(Repo.all(CalendarEvent))

        new_events =
          data
          |> Enum.map(&create_or_update(&1, mapped))
          |> Enum.reject(&is_nil/1)

        clear_old_data(mapped, new_events, :launch_date)

        new_events

      {:error, status, %{reason: reason}} ->
        Logger.error(
          "Could not connect to dates service, received #{status} status with reason: #{reason}"
        )
    end
  end

  defp create_or_update(%{"movie_id" => nil}, _current_events), do: nil

  defp create_or_update(
         data = %{"movie_id" => movie_id, "launch_date" => launch_date},
         current_events
       ) do
    found = current_events[movie_id]

    case found do
      nil ->
        insert_record(data)

      record ->
        converted = convert_date(launch_date)

        case compare_dates(Map.get(record, :launch_date), converted) == :eq do
          true -> record
          false -> update_record(record, :launch_date, converted)
        end
    end
  end

  defp convert_date(nil), do: nil

  defp convert_date(date) do
    case NaiveDateTime.from_iso8601(date) do
      {:ok, date} -> date
      {:error, _} -> nil
    end
  end

  defp compare_dates(nil, nil), do: true
  defp compare_dates(_date_1, nil), do: false
  defp compare_dates(nil, _date_2), do: false
  defp compare_dates(date_1, date_2), do: NaiveDateTime.compare(date_1, date_2)
end
