defmodule Orion.Parsers.Helpers do
  require Logger

  alias Orion.Schemas.CalendarEvent
  alias Orion.Repo

  def insert_record(data) do
    changeset = CalendarEvent.changeset(%CalendarEvent{}, data)

    Repo.insert!(changeset, on_conflict: :replace_all, conflict_target: :movie_id)
  end

  def update_record(record, property, data) do
    changeset = CalendarEvent.changeset(record, Map.put(%{}, property, data))

    Repo.update!(changeset)
  end

  def delete_property(record, property) do
    update_record(record, property, nil)
  end

  def clear_old_data(current_events, new_events, property) do
    new_event_ids = Enum.map(new_events, & &1.movie_id)

    {_inserted, should_delete} = get_diff(Map.keys(current_events), new_event_ids)

    Enum.map(should_delete, &delete_property(current_events[&1], property))
  end

  def get_diff(old_events, new_events) do
    should_insert = new_events
    should_delete = old_events -- new_events

    {should_insert, should_delete}
  end

  def mapify_events(events) do
    Enum.reduce(events, %{}, fn event, acc -> Map.put(acc, event.movie_id, event) end)
  end
end
