defmodule Orion.Contexts.CalendarEvents do
  alias Orion.Schemas.CalendarEvent

  def get() do
    calendar_events = CalendarEvent.displayable_events(CalendarEvent)

    {:ok, calendar_events}
  end

  def delete_all() do
    CalendarEvent.delete_all()
  end
end
