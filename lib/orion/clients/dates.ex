defmodule Orion.Clients.Dates do
  alias HTTPoison.{Error, Response}

  def get(_params) do
    result = HTTPoison.get(url(), process_headers())

    case result do
      {:ok, %Response{status_code: 200, body: body}} ->
        Poison.decode(body)

      {:ok, %Response{status_code: 403}} ->
        {:error, :forbidden, %{}}

      {:ok, %Response{status_code: 404}} ->
        {:error, :not_found, %{}}

      {:ok, %Response{status_code: 500}} ->
        {:error, :server_error, %{}}

      {:error, %Error{reason: reason}} ->
        {:error, :server_error, %{reason: reason}}
    end
  end

  defp auth_token(), do: Application.get_env(:orion, :dates_secret)

  defp url(), do: Application.get_env(:orion, :dates_url)

  defp process_headers() do
    [
      Authorization: auth_token(),
      "Content-Type": "application/json",
      Accept: "application/json; Charset=utf-8"
    ]
  end
end
