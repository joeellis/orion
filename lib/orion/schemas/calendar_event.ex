defmodule Orion.Schemas.CalendarEvent do
  use Ecto.Schema

  import Ecto.Changeset
  import Ecto.Query

  alias Orion.Repo

  @derive {Poison.Encoder, only: [:movie_id, :title, :launch_date, :thumbnail_url]}
  schema "calendar_events" do
    field(:movie_id, :string)
    field(:title, :string)
    field(:launch_date, :naive_datetime)
    field(:thumbnail_url, :string)

    timestamps()
  end

  def changeset(record, attrs \\ %{}) do
    cast(record, attrs, [:movie_id, :title, :launch_date, :thumbnail_url])
    |> validate_required(:movie_id)
  end

  def displayable_events(query) do
    from(
      c in query,
      where: not is_nil(c.movie_id) and not is_nil(c.title) and not is_nil(c.launch_date)
    )
    |> Repo.all()
  end

  def delete_all() do
    Repo.delete_all(__MODULE__)
  end
end
