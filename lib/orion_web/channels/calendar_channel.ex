defmodule OrionWeb.CalendarChannel do
  require Logger

  use OrionWeb, :channel

  def join("calendar:lobby", payload, socket) do
    if authorized?(payload) do
      schedule_work(0)

      {:ok, socket}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  def handle_info(:send_new_events, socket) do
    {:ok, events} = Orion.Contexts.CalendarEvents.get()

    Logger.info("Sending #{length(events)} events through the channel")

    broadcast!(socket, "new_events", %{"data" => events})

    schedule_work(3000)

    {:noreply, socket}
  end

  defp schedule_work(time) do
    Process.send_after(self(), :send_new_events, time)
  end

  defp authorized?(_payload) do
    true
  end
end
