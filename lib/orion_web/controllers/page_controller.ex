defmodule OrionWeb.PageController do
  use OrionWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end

  def delete(conn, _params) do
    Orion.Contexts.CalendarEvents.delete_all()

    redirect(conn, to: page_path(conn, :index))
  end
end
