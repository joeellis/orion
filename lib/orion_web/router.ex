defmodule OrionWeb.Router do
  use OrionWeb, :router

  pipeline :browser do
    plug(:accepts, ["html"])
    plug(:fetch_session)
    plug(:fetch_flash)
    plug(:protect_from_forgery)
    plug(:put_secure_browser_headers)
  end

  pipeline :api do
    plug(:accepts, ["json"])
  end

  scope "/", OrionWeb do
    # Use the default browser stack
    pipe_through(:browser)

    get("/", PageController, :index)
    delete("/delete", PageController, :delete)
  end
end
