# Orion

URL: https://joe-orion.herokuapp.com/

### To run locally

The system uses six env vars to get fully up and running:

- `THUMBNAILS_SECRET`: (required) auth token for the thumbnails microservice endpoint
- `TITLES_SECRET`: (required) auth token for the titles microservice endpoint
- `DATES_SECRET`: (required) auth token for the dates microservice endpoint
- `THUMBNAILS_URL`: (optional) url for the thumbnails microservice endpoint (defaults to localhost:5000/thumbnails)
- `TITLES_URL`: (optional) url for the titles microservice endpoint (defaults to localhost:5000/titles)
- `DATES_URL`: (optional) url for the dates microservice endpoint (defaults to localhost:5000/dates)

You can either point the urls to the orion_takehome heroku app or run a local instance of that server and have the app consume from that data locally.

To start the server:

- Use:
  - Elixir 1.6.1 / Erlang 20.1
  - Phoenix 1.3
  - A recent version of Node (tested on Node 9.x.x but 8 or 7 shouldn't give you issues)
- Install dependencies with `mix deps.get`
- Create and migrate your database with `mix ecto.create && mix ecto.migrate`
- Install Node.js dependencies with `cd assets && npm install`
- Start Phoenix endpoint with `mix phx.server`
- Visit [`localhost:4000`](http://localhost:4000) in the browser

Use your terminal console to see the app consume data from microservices and use the web developer console to see data being pushed through the websocket channel in realtime. You can also click the delete button to destroy the DB and watch it rebuild itself once enough valid event data has been pulled from the microservice endpoints.

**Note**: to make logging more readable, I've switched the logging level to `:info` for this demo. To see the full db activity and job scheduling logs, just switch the logging level to `:debug` in `config/dev.exs`

### Things to look at

I'd also recommend taking a look at:

- The scheduler process kicks off when the app is started in `lib/orion/application.ex` and then it runs jobs at the frequency setup in `config/config.exs`
- Each scheduled job points to the module responsible for parsing each microservice, the function to call, and the parameters to pass in (in this case, the client)
- the `clients`, `parsers`, and `schemas` folders under `lib/orion` house the implementation details for the rest of the entities

### Testing

Run `mix test` or `mix test.watch` at the root level of the directory to run tests.

### Troubleshooting

If you hit any issues or questions, please feel free to email me at joe@joeellis.la anytime - I'm really nice and happy to help!
