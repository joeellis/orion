defmodule Orion.Clients.DatesTest do
  use ExUnit.Case

  setup do
    server = Bypass.open()
    Application.put_env(:orion, :dates_url, "http://localhost:#{server.port}/dates")

    {:ok, server: server}
  end

  test "can get dates", %{server: server} do
    Bypass.expect(server, fn conn ->
      assert "/dates" == conn.request_path
      assert "GET" == conn.method

      encoded =
        Poison.encode!(%{
          data: [
            %{
              id: 1,
              movie_id: "ee3c0801-9609-49ea-87fa-fcb9b9f438b9",
              launch_date: "2017-04-28 00:00:00"
            }
          ]
        })

      Plug.Conn.resp(conn, 200, encoded)
    end)

    {:ok, response} = Orion.Clients.Dates.get(%{})

    assert Enum.at(response["data"], 0)["movie_id"] == "ee3c0801-9609-49ea-87fa-fcb9b9f438b9"
  end

  test "can handle 404s dates", %{server: server} do
    Bypass.expect(server, fn conn ->
      assert "/dates" == conn.request_path
      assert "GET" == conn.method

      Plug.Conn.resp(conn, 404, "")
    end)

    {:error, :not_found, _meta} = Orion.Clients.Dates.get(%{})
  end
end
