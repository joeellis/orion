defmodule Orion.Clients.TitlesTest do
  use ExUnit.Case

  setup do
    server = Bypass.open()
    Application.put_env(:orion, :titles_url, "http://localhost:#{server.port}/titles")

    {:ok, server: server}
  end

  test "can get titles", %{server: server} do
    Bypass.expect(server, fn conn ->
      assert "/titles" == conn.request_path
      assert "GET" == conn.method

      encoded =
        Poison.encode!(%{
          data: [
            %{
              id: 1,
              movie_id: "ee3c0801-9609-49ea-87fa-fcb9b9f438b9",
              title: "Some Title"
            }
          ]
        })

      Plug.Conn.resp(conn, 200, encoded)
    end)

    {:ok, response} = Orion.Clients.Titles.get(%{})

    assert Enum.at(response["data"], 0)["movie_id"] == "ee3c0801-9609-49ea-87fa-fcb9b9f438b9"
  end

  test "can handle 404s titles", %{server: server} do
    Bypass.expect(server, fn conn ->
      assert "/titles" == conn.request_path
      assert "GET" == conn.method

      Plug.Conn.resp(conn, 404, "")
    end)

    {:error, :not_found, _meta} = Orion.Clients.Titles.get(%{})
  end
end
