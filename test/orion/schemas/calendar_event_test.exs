defmodule Orion.Parsers.CalendarEventTest do
  use Orion.DataCase

  alias Orion.Schemas.CalendarEvent
  alias Orion.Repo

  test "can filter out incomplete calendar events in db" do
    Repo.insert!(
      CalendarEvent.changeset(%CalendarEvent{}, %{
        movie_id: "ee3c0801-9609-49ea-87fa-fcb9b9f438b9",
        title: "Some Movie",
        launch_date: ~N[2018-01-01 00:00:00]
      })
    )

    Repo.insert!(
      CalendarEvent.changeset(%CalendarEvent{}, %{
        movie_id: "8761273c-958f-49ef-bc1a-9f8022d24d11",
        title: "Some Movie 2",
        launch_date: ~N[2018-01-01 00:00:00]
      })
    )

    # invalid event
    Repo.insert!(
      CalendarEvent.changeset(%CalendarEvent{}, %{
        movie_id: "abcd-efgh",
        title: nil,
        launch_date: ~N[2018-01-01 00:00:00]
      })
    )

    assert length(CalendarEvent.displayable_events(CalendarEvent)) == 2
  end

  test "can ensure movie_id property is unique" do
    record_changeset =
      CalendarEvent.changeset(%CalendarEvent{}, %{
        movie_id: "ee3c0801-9609-49ea-87fa-fcb9b9f438b9",
        thumbnail_url: "thumbnail.jpg"
      })

    Repo.insert!(record_changeset)

    assert_raise Ecto.ConstraintError, fn ->
      Repo.insert!(record_changeset)
    end
  end
end
