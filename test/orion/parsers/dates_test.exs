defmodule Orion.Parsers.DatesTest do
  use Orion.DataCase

  alias Orion.Schemas.CalendarEvent
  alias Orion.Repo

  defmodule ClientMock do
    def get(_params) do
      {:ok,
       %{
         "data" => [
           %{
             "id" => 1,
             "movie_id" => "ee3c0801-9609-49ea-87fa-fcb9b9f438b9",
             "launch_date" => "2018-01-01 00:00:00"
           },
           %{
             "id" => 2,
             "movie_id" => "8761273c-958f-49ef-bc1a-9f8022d24d11",
             "launch_date" => "2018-01-01 00:00:00"
           },
           %{
             "id" => 3,
             "movie_id" => nil,
             "launch_date" => "2018-01-02 00:00:00"
           }
         ]
       }}
    end
  end

  setup do
    %{client: ClientMock}
  end

  test "can create calendar events for new movie records from microservice", %{client: client} do
    [movie_1, _movie_2] = Orion.Parsers.Dates.parse(client)

    assert movie_1.movie_id == "ee3c0801-9609-49ea-87fa-fcb9b9f438b9"
  end

  test "can return back existing calendar events from db", %{client: client} do
    Repo.insert!(
      CalendarEvent.changeset(%CalendarEvent{}, %{
        movie_id: "ee3c0801-9609-49ea-87fa-fcb9b9f438b9",
        launch_date: ~N[2018-01-01 00:00:00]
      })
    )

    [movie_1, _movie_2] = Orion.Parsers.Dates.parse(client)

    assert movie_1.movie_id == "ee3c0801-9609-49ea-87fa-fcb9b9f438b9"
  end

  test "can fill in missing launch date data if missing", %{client: client} do
    Repo.insert!(
      CalendarEvent.changeset(%CalendarEvent{}, %{
        movie_id: "ee3c0801-9609-49ea-87fa-fcb9b9f438b9",
        launch_date: nil
      })
    )

    [movie_1, _movie_2] = Orion.Parsers.Dates.parse(client)

    assert movie_1.launch_date == ~N[2018-01-01 00:00:00]
  end

  test "can replace launch date data if changed", %{client: client} do
    Repo.insert!(
      CalendarEvent.changeset(%CalendarEvent{}, %{
        movie_id: "ee3c0801-9609-49ea-87fa-fcb9b9f438b9",
        launch_date: ~N[2018-02-01 00:00:00]
      })
    )

    [movie_1, _movie_2] = Orion.Parsers.Dates.parse(client)

    assert movie_1.launch_date == ~N[2018-01-01 00:00:00]
  end

  test "can skip events with no movie_id", %{client: client} do
    movies = Orion.Parsers.Dates.parse(client)

    assert length(Enum.filter(movies, &(&1.id == 3))) == 0
  end

  test "can delete dates data for events not returned from service", %{client: client} do
    Repo.insert!(
      CalendarEvent.changeset(%CalendarEvent{}, %{
        movie_id: "abcd-efgh",
        launch_date: ~N[2018-01-01 00:00:00]
      })
    )

    Orion.Parsers.Dates.parse(client)

    record = Repo.get_by(CalendarEvent, movie_id: "abcd-efgh")

    assert record.launch_date == nil
  end
end
