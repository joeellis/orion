defmodule Orion.Parsers.TitlesTest do
  use Orion.DataCase

  alias Orion.Schemas.CalendarEvent
  alias Orion.Repo

  defmodule ClientMock do
    def get(%{}) do
      {:ok,
       %{
         "data" => [
           %{
             "id" => 1,
             "movie_id" => "ee3c0801-9609-49ea-87fa-fcb9b9f438b9",
             "title" => "Some Movie"
           },
           %{
             "id" => 2,
             "movie_id" => "ee3c0801-9609-49ea-87fa-fcb9b9f438b9",
             "title" => "Some Movie 2"
           },
           %{
             "id" => 3,
             "movie_id" => nil,
             "title" => "Some Movie 3"
           }
         ]
       }}
    end
  end

  setup do
    %{client: ClientMock}
  end

  test "can create calendar events for new movie records from microservice", %{client: client} do
    [movie_1, _movie_2] = Orion.Parsers.Titles.parse(client)

    assert movie_1.movie_id == "ee3c0801-9609-49ea-87fa-fcb9b9f438b9"
  end

  test "can return back existing calendar events from db", %{client: client} do
    Repo.insert!(
      CalendarEvent.changeset(%CalendarEvent{}, %{
        movie_id: "ee3c0801-9609-49ea-87fa-fcb9b9f438b9",
        title: "Some Movie"
      })
    )

    [movie_1, _movie_2] = Orion.Parsers.Titles.parse(client)

    assert movie_1.movie_id == "ee3c0801-9609-49ea-87fa-fcb9b9f438b9"
  end

  test "can fill in missing title data if missing", %{client: client} do
    Repo.insert!(
      CalendarEvent.changeset(%CalendarEvent{}, %{
        movie_id: "ee3c0801-9609-49ea-87fa-fcb9b9f438b9",
        title: nil
      })
    )

    [movie_1, _movie_2] = Orion.Parsers.Titles.parse(client)

    assert movie_1.title == "Some Movie"
  end

  test "can replace title data if changed", %{client: client} do
    Repo.insert!(
      CalendarEvent.changeset(%CalendarEvent{}, %{
        movie_id: "ee3c0801-9609-49ea-87fa-fcb9b9f438b9",
        title: "Some Movie 123"
      })
    )

    [movie_1, _movie_2] = Orion.Parsers.Titles.parse(client)

    assert movie_1.title == "Some Movie"
  end

  test "can skip events with no movie_id", %{client: client} do
    movies = Orion.Parsers.Titles.parse(client)

    assert length(Enum.filter(movies, &(&1.id == 3))) == 0
  end

  test "can delete titles data for events not returned from service", %{client: client} do
    Repo.insert!(
      CalendarEvent.changeset(%CalendarEvent{}, %{
        movie_id: "abcd-efgh",
        title: "Some Movie 5"
      })
    )

    Orion.Parsers.Titles.parse(client)

    record = Repo.get_by(CalendarEvent, movie_id: "abcd-efgh")

    assert record.launch_date == nil
  end
end
