defmodule Orion.Parsers.HelpersTest do
  use Orion.DataCase

  alias Orion.Parsers.Helpers

  test "can get diff between two result sets 1" do
    old = []
    new = [1, 2, 3]

    {[1, 2, 3], []} = Helpers.get_diff(old, new)
  end

  test "can get diff between two result sets 2" do
    old = [1, 2]
    new = [1, 2, 3, 4]

    {[1, 2, 3, 4], []} = Helpers.get_diff(old, new)
  end

  test "can get diff between two result sets 3" do
    old = [1, 2, 3, 4]
    new = [1, 2]

    {[1, 2], [3, 4]} = Helpers.get_diff(old, new)
  end

  test "can get diff between two result sets 4" do
    old = [1, 2, 3, 4]
    new = []

    {[], [1, 2, 3, 4]} = Helpers.get_diff(old, new)
  end

  test "can get diff between two result sets 5" do
    old = [1]
    new = [2, 3, 4]

    {[2, 3, 4], [1]} = Helpers.get_diff(old, new)
  end
end
