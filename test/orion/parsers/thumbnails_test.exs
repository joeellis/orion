defmodule Orion.Parsers.ThumbnailsTest do
  use Orion.DataCase

  alias Orion.Schemas.CalendarEvent
  alias Orion.Repo

  defmodule ClientMock do
    def get(%{}) do
      {:ok,
       %{
         "data" => [
           %{
             "id" => 1,
             "movie_id" => "ee3c0801-9609-49ea-87fa-fcb9b9f438b9",
             "thumbnail_url" => "thumbnail.jpg"
           },
           %{
             "id" => 2,
             "movie_id" => "8761273c-958f-49ef-bc1a-9f8022d24d11",
             "thumbnail_url" => "other_thumbnail.jpg"
           },
           %{
             "id" => 3,
             "movie_id" => nil,
             "thumbnail_url" => "some_thumbnail.jpg"
           }
         ]
       }}
    end
  end

  setup do
    %{client: ClientMock}
  end

  test "can create calendar events for new movie records from microservice", %{client: client} do
    [movie_1, _movie_2] = Orion.Parsers.Thumbnails.parse(client)

    assert movie_1.movie_id == "ee3c0801-9609-49ea-87fa-fcb9b9f438b9"
  end

  test "can return back existing calendar events from db", %{client: client} do
    Repo.insert!(
      CalendarEvent.changeset(%CalendarEvent{}, %{
        movie_id: "ee3c0801-9609-49ea-87fa-fcb9b9f438b9",
        thumbnail_url: "thumbnail.jpg"
      })
    )

    [movie_1, _movie_2] = Orion.Parsers.Thumbnails.parse(client)

    assert movie_1.movie_id == "ee3c0801-9609-49ea-87fa-fcb9b9f438b9"
  end

  test "can fill in missing thumbnail data if missing", %{client: client} do
    Repo.insert!(
      CalendarEvent.changeset(%CalendarEvent{}, %{
        movie_id: "ee3c0801-9609-49ea-87fa-fcb9b9f438b9",
        thumbnail_url: nil
      })
    )

    [movie_1, _movie_2] = Orion.Parsers.Thumbnails.parse(client)

    assert movie_1.thumbnail_url == "thumbnail.jpg"
  end

  test "can replace thumbnail data if changed", %{client: client} do
    Repo.insert!(
      CalendarEvent.changeset(%CalendarEvent{}, %{
        movie_id: "ee3c0801-9609-49ea-87fa-fcb9b9f438b9",
        thumbnail_url: "thumbnail123.jpg"
      })
    )

    [movie_1, _movie_2] = Orion.Parsers.Thumbnails.parse(client)

    assert movie_1.thumbnail_url == "thumbnail.jpg"
  end

  test "can skip events with no movie_id", %{client: client} do
    movies = Orion.Parsers.Thumbnails.parse(client)

    assert length(Enum.filter(movies, &(&1.id == 3))) == 0
  end

  test "can delete thumbnail data for events not returned from service", %{client: client} do
    Repo.insert!(
      CalendarEvent.changeset(%CalendarEvent{}, %{
        movie_id: "abcd-efgh",
        thumbnail_url: "thumbnail123.jpg"
      })
    )

    Orion.Parsers.Thumbnails.parse(client)

    record = Repo.get_by(CalendarEvent, movie_id: "abcd-efgh")

    assert record.thumbnail_url == nil
  end
end
