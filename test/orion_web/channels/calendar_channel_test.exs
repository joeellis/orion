defmodule OrionWeb.CalendarChannelTest do
  use OrionWeb.ChannelCase

  alias Orion.Schemas.CalendarEvent
  alias Orion.Repo
  alias OrionWeb.CalendarChannel

  test "broadcasts all valid calendar events" do
    event_1 =
      Repo.insert!(
        CalendarEvent.changeset(%CalendarEvent{}, %{
          movie_id: "ee3c0801-9609-49ea-87fa-fcb9b9f438b9",
          title: "Some Movie",
          launch_date: ~N[2018-01-01 00:00:00]
        })
      )

    event_2 =
      Repo.insert!(
        CalendarEvent.changeset(%CalendarEvent{}, %{
          movie_id: "01ddcfb6-9be0-4d99-a6e3-6b17ee37ac97",
          title: "Some Movie 2",
          launch_date: ~N[2018-01-02 00:00:00]
        })
      )

    {:ok, _, _socket} =
      subscribe_and_join(socket("user_id", %{}), CalendarChannel, "calendar:lobby")

    assert_broadcast("new_events", %{"data" => [pushed_event_1, pushed_event_2]})

    assert event_1.id == pushed_event_1.id
    assert event_2.id == pushed_event_2.id
  end

  test "does not broadcast invalid calendar events" do
    Repo.insert!(
      CalendarEvent.changeset(%CalendarEvent{}, %{
        movie_id: "01ddcfb6-9be0-4d99-a6e3-6b17ee37ac97",
        title: nil,
        launch_date: ~N[2018-01-01 00:00:00]
      })
    )

    Repo.insert!(
      CalendarEvent.changeset(%CalendarEvent{}, %{
        movie_id: "8761273c-958f-49ef-bc1a-9f8022d24d11",
        title: "Some Movie",
        launch_date: nil
      })
    )

    {:ok, _, _socket} =
      subscribe_and_join(socket("user_id", %{}), CalendarChannel, "calendar:lobby")

    assert_broadcast("new_events", %{"data" => []})
  end
end
