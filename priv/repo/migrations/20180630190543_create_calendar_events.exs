defmodule Orion.Repo.Migrations.CreateCalendarEvents do
  use Ecto.Migration

  def change do
    create table(:calendar_events) do
      add(:movie_id, :string, null: false)
      add(:title, :string, null: true)
      add(:launch_date, :naive_datetime, null: true)
      add(:thumbnail_url, :string, null: true)

      timestamps()
    end

    create(index(:calendar_events, [:movie_id], unique: true))
  end
end
