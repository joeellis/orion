# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :orion, ecto_repos: [Orion.Repo]

# Configures the endpoint
config :orion, OrionWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "8vqR/EjfKIxPKVclZMBhiwbrwp6g8qD8DSa1FEc1nbWGOWVZCqGb6eOp3Ph6ALAN",
  render_errors: [view: OrionWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Orion.PubSub, adapter: Phoenix.PubSub.PG2]

config :orion,
  titles_secret: System.get_env("TITLES_SECRET") || raise("TITLES_SECRET not set"),
  dates_secret: System.get_env("DATES_SECRET") || raise("DATES_SECRET not set"),
  thumbnails_secret: System.get_env("THUMBNAILS_SECRET") || raise("THUMBNAILS_SECRET not set"),
  dates_url: System.get_env("DATES_URL") || "http://localhost:5000/dates",
  titles_url: System.get_env("TITLES_URL") || "http://localhost:5000/titles",
  thumbnails_url: System.get_env("THUMBNAILS_URL") || "http://localhost:5000/thumbnails"

config :orion, Orion.Scheduler,
  jobs: [
    # run every 15 seconds
    fetch_dates: [
      schedule: {:extended, "*/15"},
      task: {Orion.Parsers.Dates, :parse, [Orion.Clients.Dates]}
    ],
    # run every 30 seconds
    fetch_titles: [
      schedule: {:extended, "*/30"},
      task: {Orion.Parsers.Titles, :parse, [Orion.Clients.Titles]}
    ],
    # run every 1 minute
    fetch_thumbnails: [
      schedule: "*/1 * * * *",
      task: {Orion.Parsers.Thumbnails, :parse, [Orion.Clients.Thumbnails]}
    ]
  ]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
