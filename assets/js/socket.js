import { Socket } from "phoenix";

let socket = new Socket("/socket", {});

socket.connect();

let channel = socket.channel("calendar:lobby", {});
channel
  .join()
  .receive("ok", resp => {
    console.log("Joined channel!");
  })
  .receive("error", resp => {
    console.log("Error joining channel!");
  });

channel.on("new_events", ({ data: events }) => {
  if (events.length > 0) {
    console.table(events, ["movie_id", "title", "launch_date", "thumbnail_url"]);
  } else {
    console.log("No valid events found...but currently consuming microservices so wait for about ~30 sec");
  }
});

export default socket;
